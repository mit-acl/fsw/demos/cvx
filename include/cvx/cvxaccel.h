#pragma once

#include <Eigen/Dense>

extern "C" {
#include "accel/solver.h"
}

namespace acl {
namespace cvx {

  class CVXAccel : public CVX
  {
  public:
    CVXAccel();
    ~CVXAccel() = default;

  private:
    /// \brief Dimensions used in CVXGEN problem description
    static constexpr int N = 20; ///< number of timesteps in horizon
    static constexpr int n = 6; ///< dim of state (x, y, z, vx, vy, vz)
    static constexpr int m = 3; ///< dim of input (ax, ay, az)

    double callOptimizer(double u_max, double x0[], double xf[]);
    int    checkConvergence(double xf[], double xf_opt[]);
    void   genNewTraj(double u_max, double xf[]);
    void   interpInput(double dt, double xf[], double u0[], double x0[], double ** u, double ** x, Eigen::MatrixXd &U, Eigen::MatrixXd &X);

    Eigen::MatrixXd U_, X_;
    bool replan_, optimized_, use_ff_;
    double u_min_, u_max_, z_start_, spinup_time_, z_land_;
  };

} // ns cvx
} // ns acl
