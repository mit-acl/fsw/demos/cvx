#!/usr/bin/env python
"""
Operator Node for CVX Trajectory Generator

This node enables an operator to send cvx/Waypoint messages to the cvx
trajectory generator using rviz goals. The altitude of the waypoints is
set via rosparam.
"""

import rospy

import numpy as np

from tf.transformations import euler_from_quaternion

from geometry_msgs.msg import PoseStamped
from visualization_msgs.msg import Marker
from cvx.msg import Waypoint


class Operator:

    def __init__(self):

        #
        # Load ROS Parameters
        # 

        # room bounds
        self.bounds = {}
        self.bounds['xmax'] = rospy.get_param('/room_bounds/x_max',  1.0)
        self.bounds['xmin'] = rospy.get_param('/room_bounds/x_min', -1.0)
        self.bounds['ymax'] = rospy.get_param('/room_bounds/y_max',  1.0)
        self.bounds['ymin'] = rospy.get_param('/room_bounds/y_min', -1.0)
        self.bounds['zmax'] = rospy.get_param('/room_bounds/z_max',  1.0)
        self.bounds['zmin'] = rospy.get_param('/room_bounds/z_min',  0.0)

        self.waypoint_z = rospy.get_param('~waypoint_z', 1.5)

        #
        # ROS pub / sub communication
        #

        self.pub_waypoint = rospy.Publisher("waypoint", Waypoint,
                                                queue_size=1, latch=True)
        self.pub_vizwaypoint = rospy.Publisher("viz_waypoint", Marker,
                                                queue_size=1, latch=True)

        self.sub_rvizgoal = rospy.Subscriber("~/move_base_simple/goal",
                                                PoseStamped, self.goalCB,
                                                queue_size=1)

    def goalCB(self, msg):
        # check that desired goal is within room bounds
        if (msg.pose.position.x < self.bounds['xmin']
                or msg.pose.position.x > self.bounds['xmax']
                or msg.pose.position.y < self.bounds['ymin']
                or msg.pose.position.y > self.bounds['ymax']):
            rospy.logerr('({:.2f}, {:.2f}) is out of bounds'.format(
                        msg.pose.position.x, msg.pose.position.y))
            return

        w = Waypoint()
        w.header.stamp = rospy.Time.now()
        w.header.frame_id = "world"
        w.pos.x = msg.pose.position.x
        w.pos.y = msg.pose.position.y
        w.pos.z = self.waypoint_z
        w.vel.x, w.vel.y, w.vel.z = 0.0, 0.0, 0.0
        _, _, w.yaw = euler_from_quaternion((
                        msg.pose.orientation.x, msg.pose.orientation.y,
                        msg.pose.orientation.z, msg.pose.orientation.w))
        self.pub_waypoint.publish(w)

        m = Marker()
        m.header = w.header
        m.id = 0
        m.type = Marker.SPHERE
        m.action = Marker.ADD
        m.color.a = 1
        m.color.r, m.color.g, m.color.b = (1,0.5,0)
        m.scale.x, m.scale.y, m.scale.z = 0.35, 0.35, 0.35
        m.pose = msg.pose; m.pose.position.z = w.pos.z
        self.pub_vizwaypoint.publish(m)


if __name__ == '__main__':

    rospy.init_node('cvx_operator', anonymous=False)
    ns = rospy.get_namespace()
    try:
        if str(ns) == '/':
            rospy.logfatal("Need to specify namespace as vehicle name.")
            rospy.logfatal("This is typically accomplished in a launch file.")
            rospy.signal_shutdown("no namespace specified")
        else:
            print "Starting cvx operator for: " + ns
            node = Operator()
            rospy.spin()
    except rospy.ROSInterruptException:
        pass
