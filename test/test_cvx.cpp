#include <iostream>
#include <memory>

#include <gtest/gtest.h>

#include "cvx/cvx.h"

class CVXTest : public ::testing::Test {
  public:
    CVXTest() {
      static constexpr double control_dt = 0.01;
      static constexpr double u_max = 2;
      static constexpr double qf = 100000;
      static constexpr double term_thr = 4000;
      cvx_.reset(new acl::cvx::CVX(control_dt, u_max, qf, term_thr));
    }

  protected:
    std::unique_ptr<acl::cvx::CVX> cvx_;

};

// ----------------------------------------------------------------------------

TEST_F(CVXTest, nullTrajectory)
{
  acl::cvx::State x0 = acl::cvx::State::Zero();
  acl::cvx::State xf = acl::cvx::State::Zero();
  acl::cvx::TrajXd traj;

  cvx_->generateTrajectory(x0, xf, traj);

  EXPECT_DOUBLE_EQ(traj.sum(), 0);
}

// ----------------------------------------------------------------------------

TEST_F(CVXTest, shortStraightLineTrajectory)
{
  acl::cvx::State x0 = acl::cvx::State::Zero();
  acl::cvx::State xf = acl::cvx::State::Zero();
  xf(0) = 0.5;

  acl::cvx::TrajXd traj;

  cvx_->generateTrajectory(x0, xf, traj);

  double error = (xf - traj.rightCols<1>().head<acl::cvx::dim::n>()).norm();

  EXPECT_NEAR(error, 0.0, 0.1);
}

// ----------------------------------------------------------------------------

TEST_F(CVXTest, longCurvedTrajectory)
{
  acl::cvx::State x0 = acl::cvx::State::Zero();
  x0(3) = -1;
  x0(4) = -2;
  acl::cvx::State xf = acl::cvx::State::Zero();
  xf(0) = 10;
  xf(1) = 3;

  acl::cvx::TrajXd traj;

  cvx_->generateTrajectory(x0, xf, traj);

  double error = (xf - traj.rightCols<1>().head<acl::cvx::dim::n>()).norm();

  EXPECT_NEAR(error, 0.0, 0.1);
}

// ----------------------------------------------------------------------------

int main(int argc, char **argv){
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
