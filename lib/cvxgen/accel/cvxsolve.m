% Produced by CVXGEN, 2019-03-25 15:58:43 -0400.
% CVXGEN is Copyright (C) 2006-2017 Jacob Mattingley, jem@cvxgen.com.
% The code in this file is Copyright (C) 2006-2017 Jacob Mattingley.
% CVXGEN, or solvers produced by CVXGEN, cannot be used for commercial
% applications without prior written permission from Jacob Mattingley.

% Filename: cvxsolve.m.
% Description: Solution file, via cvx, for use with sample.m.
function [vars, status] = cvxsolve(params, settings)
A = params.A;
B = params.B;
Q_final = params.Q_final;
u_max = params.u_max;
x_0 = params.x_0;
xf = params.xf;
cvx_begin
  % Caution: automatically generated by cvxgen. May be incorrect.
  variable u_1(3, 1);
  variable u_2(3, 1);
  variable u_3(3, 1);
  variable u_4(3, 1);
  variable u_5(3, 1);
  variable u_6(3, 1);
  variable u_7(3, 1);
  variable u_8(3, 1);
  variable u_9(3, 1);
  variable u_10(3, 1);
  variable u_11(3, 1);
  variable u_12(3, 1);
  variable u_13(3, 1);
  variable u_14(3, 1);
  variable u_15(3, 1);
  variable u_16(3, 1);
  variable u_17(3, 1);
  variable u_18(3, 1);
  variable u_19(3, 1);
  variable x_20(6, 1);
  variable x_1(6, 1);
  variable u_0(3, 1);
  variable x_2(6, 1);
  variable x_3(6, 1);
  variable x_4(6, 1);
  variable x_5(6, 1);
  variable x_6(6, 1);
  variable x_7(6, 1);
  variable x_8(6, 1);
  variable x_9(6, 1);
  variable x_10(6, 1);
  variable x_11(6, 1);
  variable x_12(6, 1);
  variable x_13(6, 1);
  variable x_14(6, 1);
  variable x_15(6, 1);
  variable x_16(6, 1);
  variable x_17(6, 1);
  variable x_18(6, 1);
  variable x_19(6, 1);
  variable u_20(3, 1);

  minimize(quad_form(u_1, eye(3)) + quad_form(u_2, eye(3)) + quad_form(u_3, eye(3)) + quad_form(u_4, eye(3)) + quad_form(u_5, eye(3)) + quad_form(u_6, eye(3)) + quad_form(u_7, eye(3)) + quad_form(u_8, eye(3)) + quad_form(u_9, eye(3)) + quad_form(u_10, eye(3)) + quad_form(u_11, eye(3)) + quad_form(u_12, eye(3)) + quad_form(u_13, eye(3)) + quad_form(u_14, eye(3)) + quad_form(u_15, eye(3)) + quad_form(u_16, eye(3)) + quad_form(u_17, eye(3)) + quad_form(u_18, eye(3)) + quad_form(u_19, eye(3)) + quad_form(x_20 - xf, Q_final));
  subject to
    x_1 == A*x_0 + B*u_0;
    x_2 == A*x_1 + B*u_1;
    x_3 == A*x_2 + B*u_2;
    x_4 == A*x_3 + B*u_3;
    x_5 == A*x_4 + B*u_4;
    x_6 == A*x_5 + B*u_5;
    x_7 == A*x_6 + B*u_6;
    x_8 == A*x_7 + B*u_7;
    x_9 == A*x_8 + B*u_8;
    x_10 == A*x_9 + B*u_9;
    x_11 == A*x_10 + B*u_10;
    x_12 == A*x_11 + B*u_11;
    x_13 == A*x_12 + B*u_12;
    x_14 == A*x_13 + B*u_13;
    x_15 == A*x_14 + B*u_14;
    x_16 == A*x_15 + B*u_15;
    x_17 == A*x_16 + B*u_16;
    x_18 == A*x_17 + B*u_17;
    x_19 == A*x_18 + B*u_18;
    x_20 == A*x_19 + B*u_19;
    norm(u_0, inf) <= u_max;
    norm(u_1, inf) <= u_max;
    norm(u_2, inf) <= u_max;
    norm(u_3, inf) <= u_max;
    norm(u_4, inf) <= u_max;
    norm(u_5, inf) <= u_max;
    norm(u_6, inf) <= u_max;
    norm(u_7, inf) <= u_max;
    norm(u_8, inf) <= u_max;
    norm(u_9, inf) <= u_max;
    norm(u_10, inf) <= u_max;
    norm(u_11, inf) <= u_max;
    norm(u_12, inf) <= u_max;
    norm(u_13, inf) <= u_max;
    norm(u_14, inf) <= u_max;
    norm(u_15, inf) <= u_max;
    norm(u_16, inf) <= u_max;
    norm(u_17, inf) <= u_max;
    norm(u_18, inf) <= u_max;
    norm(u_19, inf) <= u_max;
    norm(u_20, inf) <= u_max;
cvx_end
vars.u_0 = u_0;
vars.u_1 = u_1;
vars.u{1} = u_1;
vars.u_2 = u_2;
vars.u{2} = u_2;
vars.u_3 = u_3;
vars.u{3} = u_3;
vars.u_4 = u_4;
vars.u{4} = u_4;
vars.u_5 = u_5;
vars.u{5} = u_5;
vars.u_6 = u_6;
vars.u{6} = u_6;
vars.u_7 = u_7;
vars.u{7} = u_7;
vars.u_8 = u_8;
vars.u{8} = u_8;
vars.u_9 = u_9;
vars.u{9} = u_9;
vars.u_10 = u_10;
vars.u{10} = u_10;
vars.u_11 = u_11;
vars.u{11} = u_11;
vars.u_12 = u_12;
vars.u{12} = u_12;
vars.u_13 = u_13;
vars.u{13} = u_13;
vars.u_14 = u_14;
vars.u{14} = u_14;
vars.u_15 = u_15;
vars.u{15} = u_15;
vars.u_16 = u_16;
vars.u{16} = u_16;
vars.u_17 = u_17;
vars.u{17} = u_17;
vars.u_18 = u_18;
vars.u{18} = u_18;
vars.u_19 = u_19;
vars.u{19} = u_19;
vars.u_20 = u_20;
vars.u{20} = u_20;
vars.x_1 = x_1;
vars.x{1} = x_1;
vars.x_2 = x_2;
vars.x{2} = x_2;
vars.x_3 = x_3;
vars.x{3} = x_3;
vars.x_4 = x_4;
vars.x{4} = x_4;
vars.x_5 = x_5;
vars.x{5} = x_5;
vars.x_6 = x_6;
vars.x{6} = x_6;
vars.x_7 = x_7;
vars.x{7} = x_7;
vars.x_8 = x_8;
vars.x{8} = x_8;
vars.x_9 = x_9;
vars.x{9} = x_9;
vars.x_10 = x_10;
vars.x{10} = x_10;
vars.x_11 = x_11;
vars.x{11} = x_11;
vars.x_12 = x_12;
vars.x{12} = x_12;
vars.x_13 = x_13;
vars.x{13} = x_13;
vars.x_14 = x_14;
vars.x{14} = x_14;
vars.x_15 = x_15;
vars.x{15} = x_15;
vars.x_16 = x_16;
vars.x{16} = x_16;
vars.x_17 = x_17;
vars.x{17} = x_17;
vars.x_18 = x_18;
vars.x{18} = x_18;
vars.x_19 = x_19;
vars.x{19} = x_19;
vars.x_20 = x_20;
vars.x{20} = x_20;
status.cvx_status = cvx_status;
% Provide a drop-in replacement for csolve.
status.optval = cvx_optval;
status.converged = strcmp(cvx_status, 'Solved');
