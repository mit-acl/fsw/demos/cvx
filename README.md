CVX Trajectory Generator
========================

This ROS package provides a linear model predictive control (MPC) based trajectory generator. The MPC problem is solved online using [CVXGEN](https://cvxgen.com/). Generated trajectories can then be tracked using the [`outer_loop`](https://gitlab.com/mit-acl/fsw/snap-stack/outer_loop) package.

## Getting Started

### Dependencies

You will need [`snapstack_msgs`](https://gitlab.com/mit-acl/fsw/snap-stack/snapstack_msgs) in your catkin workspace.

CVX uses the [`behavior_selector`](https://gitlab.com/mit-acl/fsw/behavior_selector) to change the flight mode between takeoff, land, and emergency kill. Make sure to have this ROS package in your workspace (only necessary for the base station).

### Running

There are two parts to running this demo. The main `cvx` node runs onboard the vehicle. An optional GUI runs on a base station with an `operator.py` script that allows waypoints to be generated in `rviz`, as shown below.

<div align="center"><img src=".gitlab/rviz.png" alt="rviz screenshot" /></div>

*Note: In simulation, both parts run on your machine.*

#### Onboard

```bash
roslaunch cvx onboard.launch
```

#### Base Station

```bash
roslaunch cvx basestation.launch
```

**Note**: In both cases make sure you have the appropriate `veh:=` and `num:=` flags set.

## Theory

One useful way of thinking about MPC is as an extension to the [linear-quadratic regulator (LQR)](https://en.wikipedia.org/wiki/Linear%E2%80%93quadratic_regulator#Finite-horizon,_discrete-time_LQR), which comes from optimal control theory. In addition to minimizing some cost functional w.r.t linear system dynamics, MPC allows for other constraints, such as state keep-out regions (i.e., obstacle avoidance) or actuator constraints (i.e., physical limitations). This is done over some horizon. In this implementation, the discrete-time optimization of a linear time-invariant (LTI) system is formally written as
```math
\begin{aligned}
& \underset{\{\mathbf{u}_i\}_{i=0}^N,\,\{\mathbf{x}_i\}_{i=1}^N}{\text{minimize}}
& & \sum_{i=0}^N \mathbf{u}_i^\top \mathbf{u}_i + (\mathbf{x}_N - \mathbf{x}_f)^\top\mathbf{Q}_f(\mathbf{x}_N - \mathbf{x}_f) \\
& \text{subject to}
& & \mathbf{x}_{i+1} = \mathbf{A}_d \mathbf{x}_i + \mathbf{B}_d \mathbf{u}_i, && i=0,\dots,N-1 \\
&&& \|\mathbf{u}_i\|_\infty < u_\text{max}, && i=0,\dots,N
\end{aligned}
```
where $`N`$ is the horizon, $`\mathbf{u}_i\in\mathbb{R}^m`$ are the optimized control inputs to the LTI system, $`\mathbf{x}_i\in\mathbb{R}^n`$ are the system states due to driving the LTI system with the optimized control, $`\mathbf{Q}_f`$ is a real positive semidefinite $`n\times n`$ matrix that weighs the terminal cost (i.e., a soft constraint) of ending away from $`\mathbf{x}_f`$, $`u_\text{max}\in\mathbb{R}`$ is the actuation limit, and the pair $`(\mathbf{A}_d,\mathbf{B}_d)`$ make up the discretized LTI system used for trajectory planning.

Note that $`\mathbf{x}_0`$ is not a design variable. When a new waypoint $`\mathbf{x}_f`$ is sent to the planner, $`\mathbf{x}_0`$ is set as the current state.

### Planning Model

The `outer_loop` levarages the differential flatness of multirotors to track trajectories. This means we can send a smooth polynomial trajectory and its derivatives to the `outer_loop`. In general, by optimizing the $`k^\text{th}`$ derivative of a trajectory, the $`(k-2)^\text{th}`$ derivative well be tracked well.

Thus, our goal is to generate a smooth trajectory. For the basic implementation, we choose $`k=2`$, i.e., acceleration.

#### Acceleration

We model the system as a first-order integrator with acceleration input (i.e., nearly-constant acceleration (NCA)). In continuous time, this is written in state-space form as

```math
\begin{bmatrix}\dot{x}\\\dot{y}\\\dot{v_x}\\\dot{v_y}\end{bmatrix} =
\begin{bmatrix}0&0&1&0\\0&0&0&1\\0&0&0&0\\0&0&0&0\end{bmatrix}
\begin{bmatrix}x\\y\\v_x\\v_y\end{bmatrix} +
\begin{bmatrix}0&0\\0&0\\1&0\\0&1\end{bmatrix}
\begin{bmatrix}a_x\\a_y\end{bmatrix}.
```

which can then be [discretized ](https://en.wikipedia.org/wiki/Discretization#Discretization_of_linear_state_space_models) with a period of $`\Delta t`$ into the following $`(\mathbf{A}_d,\mathbf{B}_d)`$ pair

```math
\mathbf{A}_d =
\begin{bmatrix}
1&0&\Delta t&0\\
0&1&0&\Delta t\\
0&0&1&0\\
0&0&0&1\\
\end{bmatrix};\qquad
\mathbf{B}_d =
\begin{bmatrix}
\frac{1}{2}\Delta t^2&0\\
0&\frac{1}{2}\Delta t^2\\
\Delta t&0\\
0&\Delta t\\
\end{bmatrix}.
```

This discretized LTI system is then used to solve the MPC problem using CVXGEN.

*Note: The system is written in 2D only for presentation, the implementation is 3D.*

#### Jerk

A jerk model can also be used, though it is not currently implemented.

## FAQ

1. How do I run the unit tests?

    - With `catkin build`: `catkin run_tests --no-deps cvx`.
